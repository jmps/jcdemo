package io.adrian.nested.lockout;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class TestBlockingQueue {
	public static void main(String[] args) {

		final Q q = new Q();

		Thread producer = new Thread() {
			@Override
			public void run() {
				try {
					q.produce();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		Thread consumer = new Thread() {
			@Override
			public void run() {
				try {
					q.consume();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		producer.start();
		consumer.start();
	}

	static class Q {
		
		private static int UNSUFFICIENT_CAPACITY = 2;
		private static int EXPECT_CAPACITY = 3; // 3 > 2
		
		private BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(UNSUFFICIENT_CAPACITY);

		public synchronized void produce() throws InterruptedException {
			for (int i = 0; i < EXPECT_CAPACITY; i++) {
				queue.put(i);
			}
		}

		public synchronized int consume() throws InterruptedException {
			int count = 0;
			for (int i = 0; i < EXPECT_CAPACITY; i++) {
				queue.take();
				count++;
			}
			return count;
		}
	}

}
