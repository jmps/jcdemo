package io.adrian.nested.lockout;

import io.adrian.nested.ThreadUtil;
import io.adrian.nested.lockout.Lock;

public class TestLock {
	public static void main(String[] args) {
		final Lock lock = new Lock();
		Runnable t1 = new Runnable() {
			@Override
			public void run() {
				try {
					lock.lock();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		Runnable t2 = new Runnable() {
			@Override
			public void run() {
				lock.unlock();
			}
		};
		
		new Thread(t1).start();
		ThreadUtil.spinMs(1000);
		new Thread(t2).start();
	}
}
