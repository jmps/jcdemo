package io.adrian.nested.lockout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Lock {
	private static final Logger log = LoggerFactory.getLogger(Lock.class);
	protected Object monitorObject = new Object();
	protected boolean isLocked = true;

	public void lock() throws InterruptedException {
		log.info("lock this {");
		synchronized (this) {
			while (isLocked) {
				synchronized (this.monitorObject) {
					this.monitorObject.wait();
				}
			}
			isLocked = true;
		}
		log.info("} lock this");
	}

	public void unlock() {
		synchronized (this) {
			this.isLocked = false;
			synchronized (this.monitorObject) {
				this.monitorObject.notify();
			}
		}
	}
}
