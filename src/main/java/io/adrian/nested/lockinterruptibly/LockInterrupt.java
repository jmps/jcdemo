package io.adrian.nested.lockinterruptibly;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import io.adrian.nested.ThreadUtil;

public class LockInterrupt {
	public static void main(String[] args) throws InterruptedException {
		final Lock lock = new ReentrantLock();

		final ScheduledThreadPoolExecutor monitorService = new ScheduledThreadPoolExecutor(10);
		monitorService.setMaximumPoolSize(20);
		monitorService.setKeepAliveTime(30, TimeUnit.SECONDS);

		final Thread adopter = new Thread() {
			@Override
			public void run() {
				try {
					System.out.println("adopter: start.");
					lock.lockInterruptibly();
					System.out.println("adopter: entry.");
					try {
						ThreadUtil.spinMs(50000);
						System.out.println("adopter: done.");
					} finally {
						lock.unlock();
					}
				} catch (InterruptedException e) {
					System.err.println("adopter: interrupted.");
				}
			}
		};

		final Thread admin = new Thread() {
			@Override
			public void run() {
				System.out.println("admin: start.");
				lock.lock();
				System.out.println("admin: entry.");
				try {
					ThreadUtil.spinMs(5000);
					System.out.println("admin: shutdown thread pool.");
					monitorService.shutdownNow();
				} finally {
					lock.unlock();
				}
			}
		};

		admin.start(); // lock first
		Thread.sleep(1000);
		monitorService.scheduleWithFixedDelay(adopter, 0, 30, TimeUnit.SECONDS); // adopter never entry
	}

}
