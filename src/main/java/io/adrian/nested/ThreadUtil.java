package io.adrian.nested;

public class ThreadUtil {
	public static void spinMs(long mils) {
		long s = System.currentTimeMillis();
		while (true) {
			long e = System.currentTimeMillis();
			if (e - s > mils)
				break;
		}
	}
}
